# React Demo Course
Purpose of this project is to gently nudge people into React development without scaring them off. Which is surprisingly hard these days.

## Overview

We are going to build an team page. With list of employees, departments, roles and stuff.

## Storybook
[Storybook](https://github.com/kadirahq/react-storybook) is a tool to test and write components without coupling them to your application

![Storybook](https://github.com/kadirahq/react-storybook/raw/master/docs/demo.gif)

We will use create a number of components, using story-book to help.

## User stories

You can't have a product without defining what it is you want to make. We use the form:
> "As a $role I can $functionality so that $optionalValue"

### Employee

- As a guest, I can see name on an employee
- As a guest, I can see a profile-picture of an employee
- As a guest, I can see a job title of an employee

### Company View

- As a guest, I can see a list of all employees
- As a guest, I can filter list of employees by tag
- As a guest, I can filter a list of employees by product


### Employee Opened

- As a guest, I can click on a contact to open a card
- As a guest, I can see employee back story on opened card
- As a guest, I can close the card again so I can return to the overview

## Data

We will emulate an AJAX call, that returns a list of employees in JSON.

### Real data lives in employees.mock.json

Format example:
```js
{
  employees: [
    {
      name: "Andri Óskarsson",
      title: "Senior Developer",
      image: "http://example.com/avatar/andri.jpg",
      backstory: "Like, totally!",
      tags: [ "react", "frontend", "js"]
      products: [ "MinUddannelse" ]
    }
  ]
}
```

## Getting Started
Install Node.js, if you don't have it already.

> npm install
