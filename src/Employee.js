import React, { PropTypes } from 'react';

export default class User extends React.PureComponent {
  static propTypes = {
    name: PropTypes.string.isRequired,
    image: PropTypes.string
  }

  static defaultProps = {
    image: 'http://i.pravatar.cc/300',
    title: 'Undefined Title'
  }

  // UI elements from Semantic UI: http://semantic-ui.com/

  render() {
    return (
      <div className="ui card">
        <div className="image">
          <img alt="User avatar" src={this.props.image}/>
        </div>
        <div className="content">
          <a className="header">{this.props.name}</a>
        </div>
      </div>
    )
  }
}
