import React, { Component } from 'react';
import './App.css';
import User from './User'

let users = [
  {
    name: 'Kristoffer',
    image: 'https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAbdAAAAJDcxYTllZThlLTQ2YTYtNDMzMy1iZWZkLTMxYjhhMDE0Mzk5NQ.jpg',
    erGlad: false
  },
  {
    name: 'Wouter',
    image: 'https://www.gravatar.com/avatar/bb4c710d49ea2432f309ff9adb665f4c?s=328&d=identicon&r=PG',
    erGlad: false
  }
]

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-intro">
          {
            users.map((user, idx) => {
              return (
                <User
                  name={user.name}
                  image={user.image}
                  key={idx}
                  happy={user.erGlad}
                  onGreet={(e) => {
                    user.erGlad = !user.erGlad
                    this.forceUpdate()
                  }}
                />
              )
            })
          }
        </div>
      </div>
    );
  }
}

export default App;
