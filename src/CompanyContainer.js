import React, { PropTypes } from 'react';

import Employee from './Employee'

// Container is (almost) the only place where we store state
export default class Companycontainer extends React.Component {
  static propTypes = {
    onRequestEmployees: PropTypes.func.isRequired // We expect a promise
  }

  state = {
    employees: []
  }

  /**
   * componentDidMount - Is run automatically by React before component is rendered for first time.
   * This life-cycle-method is usally used to fetch data on container components
   */
  componentDidMount() {
    this.props.onRequestEmployees && this.props.onRequestEmployees()
      .then((response) => {
        this.setState({
          employees: response.employees
        })
      });
  }

  render() {
    // TODO: List all employees using Employee component
    // TODO: Show loader when employees is empty
    // TODO: Maybe move Employee list into a seperate component?

    return (
      <div>
        <h1>UVdata medarbejdere</h1>

        <h2>Hov... der mangler noget</h2>

        { this.state.employees.map((employee) => (
          <div>
            <h3>{employee.name}</h3>
          </div>
        ))}
      </div>
    );
  }
}
